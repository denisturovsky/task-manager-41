package ru.tsc.denisturovsky.tm.api.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.enumerated.Status;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDTO add(@NotNull ProjectDTO model) throws Exception;

    @NotNull
    ProjectDTO add(
            @Nullable String userId,
            @NotNull ProjectDTO model
    ) throws Exception;

    void changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void changeProjectStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws Exception;

    void clear(@Nullable String userId) throws Exception;

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    ) throws Exception;

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name
    ) throws Exception;

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(
            @Nullable String userId,
            @Nullable Comparator comparator
    ) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    ) throws Exception;

    @Nullable
    ProjectDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

    @Nullable
    ProjectDTO findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void remove(
            @Nullable String userId,
            @Nullable ProjectDTO model
    ) throws Exception;

    void removeOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

    void removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws Exception;

    void updateOneById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void updateOneByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}
