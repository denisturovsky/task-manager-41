package ru.tsc.denisturovsky.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, date, user_id, role)" +
            " VALUES (#{session.id}, #{session.date}, #{session.userId}, #{session.role})")
    void add(@NotNull @Param("session") SessionDTO session);

    @Insert("INSERT INTO tm_session (id, date, user_id, role)" +
            " VALUES (#{session.id}, #{session.date}, #{userId}, #{session.role})")
    void addWithUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("session") SessionDTO session
    );

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    List<SessionDTO> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_session WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable SessionDTO findOneById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable SessionDTO findOneByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Select("SELECT COUNT(*) FROM tm_session WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_session WHERE user_id = #{session.userId} AND id = #{session.id}")
    void remove(@NotNull @Param("session") SessionDTO session);

    @Update("UPDATE tm_session SET name = #{session.name}, date = #{session.date}, role = #{session.role} WHERE id = #{session.id}")
    void update(@NotNull @Param("session") SessionDTO session);

}