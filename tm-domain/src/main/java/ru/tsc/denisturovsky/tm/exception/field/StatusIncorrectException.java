package ru.tsc.denisturovsky.tm.exception.field;

public final class StatusIncorrectException extends AbstractFieldException {

    public StatusIncorrectException() {

        super("Error! Status is incorrect...");
    }

}
