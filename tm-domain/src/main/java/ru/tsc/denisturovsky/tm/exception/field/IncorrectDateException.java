package ru.tsc.denisturovsky.tm.exception.field;

public final class IncorrectDateException extends AbstractFieldException {

    public IncorrectDateException() {

        super("Error! Incorrect date...");
    }

}
